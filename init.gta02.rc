on init

    mkdir /data/system/wpa_supplicant 0770 wifi wifi
    mkdir /data/misc/wifi/sockets 0770 wifi wifi
    mkdir /data/misc/dhcp 0770 dhcp dhcp
    mkdir /data/ppp 0777 system system

    # Make sure permissions are correct
    chown system system /data/app
    chmod 0771 /data/app
    chmod 0770 /data/misc/wifi/sockets

    # change permissions for alsa nodes
    symlink /dev/snd/pcmC0D0c /dev/pcmC0D0c
    symlink /dev/snd/pcmC0D0p /dev/pcmC0D0p
    symlink /dev/snd/controlC0 /dev/controlC0
    symlink /dev/snd/timer /dev/timer
    chmod 0660 /dev/pcmC0D0c
    chmod 0660 /dev/pcmC0D0p
    chmod 0660 /dev/controlC0
    chmod 0660 /dev/timer
    # bt-pcm p/c device nodes
    chmod 0660 /dev/snd/pcmC0D1p
    chmod 0660 /dev/snd/pcmC0D1c
    chown root audio /dev/pcmC0D0c
    chown root audio /dev/pcmC0D0p
    chown root audio /dev/controlC0
    chown root audio /dev/timer
    # bt-pcm p/c device nodes
    chown root audio /dev/snd/pcmC0D1p
    chown root audio /dev/snd/pcmC0D1c
    chown wifi wifi /sys/bus/platform/drivers/s3c2440-sdi/bind
    chown wifi wifi /sys/bus/platform/drivers/s3c2440-sdi/unbind

    # Turn off GSM
    write /sys/class/i2c-adapter/i2c-0/0-0073/neo1973-pm-gsm.0/power_on 0

    # Turn on GSM
    write /sys/bus/platform/devices/neo1973-pm-gsm.0/power_on 1

    # Turn off GPS.
    #
    # To ensure that it is in low power mode, we need to turn it
    # ON befure turning it OFF
    write /sys/bus/platform/devices/neo1973-pm-gps.0/power_on 1
    write /sys/bus/platform/devices/neo1973-pm-gps.0/power_on 0

    # Turn off WIFI
    write /sys/bus/platform/drivers/s3c2440-sdi/unbind s3c2440-sdi

    # Turn off bluetooth
    write /sys/class/rfkill/rfkill1/state 0

    # Use LEDs with a timer trigger
    write /sys/class/leds/gta02-aux:red/trigger timer
    write /sys/class/leds/gta02-power:orange/trigger timer
    write /sys/class/leds/gta02-power:blue/trigger timer

    # GSM sometimes gets stuck, so hit it twice
    write /sys/bus/platform/devices/neo1973-pm-gsm.0/power_on 1

    # Set scaling of accelerometers
    write /sys/bus/spi/drivers/lis302dl/spi3.0/full_scale 2.3
    write /sys/bus/spi/drivers/lis302dl/spi3.1/full_scale 2.3
    write /sys/bus/spi/drivers/lis302dl/spi3.1/sample_rate 400
    write /sys/bus/spi/drivers/lis302dl/spi3.0/sample_rate 400
    write /sys/bus/spi/drivers/lis302dl/spi3.1/threshold 100
    write /sys/bus/spi/drivers/lis302dl/spi3.0/threshold 100
    chown system system /sys/bus/spi/drivers/lis302dl/spi3.1/threshold
    chown system system /sys/bus/spi/drivers/lis302dl/spi3.0/threshold

    # Effectively turn off accelerometers. This is required in Cupcake until
    # a new driver can be written to conform to the new HAL in libhardware
    # because the accelerometers are interpreted as a general input device.
    #write /sys/bus/spi/drivers/lis302dl/spi3.0/threshold 8000
    #write /sys/bus/spi/drivers/lis302dl/spi3.1/threshold 8000

on boot

    #ifup usb0
    # Setup the DNS server for USB
    #setprop net.dns1 192.168.0.200

    mount vfat /dev/block/mmcblk0p1 /sdcard nosuid nodev
    setprop EXTERNAL_STORAGE_STATE mounted

    chown radio system /sys/power/wake_full_lock
    chmod 0660 /sys/power/wake_full_lock

    chown system system /sys/class/backlight/gta02-bl/brightness
    chown system system /sys/class/leds/gta02-aux:red/brightness
    chown system system /sys/class/leds/gta02-aux:red/delay_on
    chown system system /sys/class/leds/gta02-aux:red/delay_off
    chown system system /sys/class/leds/gta02-power:orange/brightness
    chown system system /sys/class/leds/gta02-power:orange/delay_on
    chown system system /sys/class/leds/gta02-power:orange/delay_off
    chown system system /sys/class/leds/gta02-power:blue/brightness
    chown system system /sys/class/leds/gta02-power:blue/delay_on
    chown system system /sys/class/leds/gta02-power:blue/delay_off
    chown system system /sys/class/leds/neo1973:vibrator/brightness

    # Open up access to GSM interface
    chown radio /dev/s3c2410_serial0

    # Open up access to GPS interface
    chmod 0666 /dev/s3c2410_serial1
    chmod 0666 /sys/bus/platform/devices/neo1973-pm-gps.0/power_on

    # Open up access to bluetooth interface
    chown bluetooth bluetooth /sys/class/rfkill/rfkill1/type
    chown bluetooth bluetooth /sys/class/rfkill/rfkill1/state
    chmod 0660 /sys/class/rfkill/rfkill1/state

    # Make PPP script executable
    chmod 0755 /etc/ppp/init.gprs-pppd

    #setprop alsa.mixer.playback.master Front
    #setprop alsa.mixer.capture.master Capture
    #setprop alsa.mixer.playback.earpiece Master
    #setprop alsa.mixer.capture.earpiece Capture
    #setprop alsa.mixer.playback.headset Master
    #setprop alsa.mixer.playback.speaker Master

    setprop alsa.mixer.playback.master Speaker
    setprop alsa.mixer.capture.master Capture
    setprop alsa.mixer.playback.earpiece Speaker
    setprop alsa.mixer.capture.earpiece Capture
    setprop alsa.mixer.playback.headset Headset
    setprop alsa.mixer.playback.speaker Speaker

    # Disable Calypso Deep Sleep mode.
    # Set to 1 for phones that have the 1024 hardware fix
    setprop freerunner.deep.sleep.enable 0

service vchanneld-daemon /system/bin/vchanneld
    user root
    group radio cache inet misc audio

## Configure the USB interface
##
service usb-ether /system/bin/ifconfig usb0 192.168.0.202
    oneshot

service pppd_gprs /system/bin/pppd debug file /data/ppp/options.gprs
    disabled
    oneshot

service wpa_supplicant /system/bin/wpa_supplicant \
    -Dwext -ieth0 -c/data/misc/wifi/wpa_supplicant.conf -dd
#    user root
#    group wifi inet keystore
    disabled

service dhcpcd /system/bin/dhcpcd -d -f /system/etc/dhcpcd/android.conf eth0
    disabled
    oneshot

service start_wifi /system/bin/start_wifi
    disabled
    oneshot

on property:vchanneld.status=start
    start ril-daemon

on property:init.svc.wpa_supplicant=stopped
    stop dhcpcd
    write /sys/bus/platform/drivers/s3c2440-sdi/unbind s3c2440-sdi

#on property:init.svc.wpa_supplicant=restarting
#    write /sys/bus/platform/drivers/s3c2440-sdi/bind s3c2440-sdi

#on property:wlan.driver.action=load
#    write /sys/bus/platform/drivers/s3c2440-sdi/bind s3c2440-sdi
#    setprop wlan.driver.action -1

#on property:wlan.driver.action=unload
#    write /sys/bus/platform/drivers/s3c2440-sdi/unbind s3c2440-sdi
#    setprop wlan.driver.action -1
