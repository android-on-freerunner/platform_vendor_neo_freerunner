# config.mk
# 
# Product-specific compile-time definitions.
#
TARGET_CPU_ABI := armeabi
TARGET_ARCH_VARIANT := armv4t
TARGET_ARCH_VERSION := armv4t
TARGET_TOOLS_PREFIX := prebuilt/$(HOST_PREBUILT_TAG)/toolchain/armv4t-android-eabi-4.2.1/bin/armv4t-android-eabi-

WITH_JIT := false
JS_ENGINE := jsc

# Wifi related defines
BOARD_WPA_SUPPLICANT_DRIVER := WEXT
WIFI_DRIVER_MODULE_PATH     := "/system/lib/modules/ar6000.ko"
WIFI_DRIVER_MODULE_ARG      := ""
WIFI_DRIVER_MODULE_NAME     := "ar6000"
WIFI_FIRMWARE_LOADER        := "start_wifi"

BOARD_HAVE_BLUETOOTH    := true
BOARD_USES_ALSA_AUDIO	:= true
BUILD_WITH_ALSA_UTILS := true
BOARD_HAVE_FREERUNNER_GPS := true
HAVE_HTC_AUDIO_DRIVER	:= false
USE_PRODUCT_WIFI_CONF	:= true
CONFIG_CTRL_IFACE       := y 
TARGET_NO_BOOTLOADER	:= true
TARGET_NO_KERNEL		:= true
TARGET_PROVIDES_INIT_RC	:= true
TARGET_HARDWARE_3D := false
USE_CAMERA_STUB			:= true
USE_LED_TYPE			:= generic
USE_QEMU_GPS_HARDWARE	:= false
USE_SENSOR_TYPE			:= moko
USE_VIBRATOR_TYPE		:= led
#This builds Android with the PacketVideo Player and codecs that
#require licensing. Comment the line below to include these codecs.
#BUILD_WITHOUT_PV=true
# The jpeg assembly doesn't currently suport armv4t
ANDROID_JPEG_NO_ASSEMBLER	:= true

BOARD_GPS_LIBRARIES := libfreerunner_gps

# Do not build iptables
override BUILD_IPTABLES		:= 0
