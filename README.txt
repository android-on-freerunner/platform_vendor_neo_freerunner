
Author: Sean McNeil

The "moko" product defines a target for the Openmoko GTA02
without a kernel or bootloader.

It depends on ALSA sound as well as a number of moko-specific
hardware changes.

It also requires ARMv4 changes.
