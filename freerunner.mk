PRODUCT_PACKAGES := \
    AccountAndSyncSettings \
    AlarmProvider \
    ApiDemos \
    ApplicationsProvider \
    Bluetooth \
    Browser \
    Bugreport \
    Calculator \
    Calendar \
    CalendarProvider \
    Camera \
    CertInstaller \
    Contacts \
    ContactsProvider \
    CubeLiveWallpapers \
    CustomLocale \
    DeskClock \
    DownloadProvider \
    DrmProvider \
    Email \
    Fallback \
    FieldTest \
    Gallery \
    GestureBuilder \
    GlobalSearch \
    GoogleSearch \
    GPSEnable \
    HTMLViewer \
    IM \
    LatinIME \
    Launcher \
    LiveWallpapersPicker \
    MediaProvider \
    Mms \
    Music \
    PackageInstaller \
    Phone \
    PicoTts \
    Provision \
    SdkSetup \
    Settings \
    SettingsProvider \
    SoftKeyboard \
    SoundRecorder \
    SubscribedFeedsProvider \
    Sync \
    SyncProvider \
    TelephonyProvider \
    Term \
    TtsService \
    Updater \
    UserDictionaryProvider \
    VoiceDialer \
    VpnServices \
    WebSearchProvider \
    framework-res 

$(call inherit-product, build/target/product/generic.mk)

# Overrides
PRODUCT_BRAND            := neo
PRODUCT_BRANDING_PARTNER := neo
PRODUCT_NAME             := fr
PRODUCT_DEVICE           := fr
PRODUCT_MANUFACTURER     := Openmoko

SYSTEM_IMAGE_FS := jffs2
USERDATA_IMAGE_FS := tar

# This is the list of locales included in the build
PRODUCT_LOCALES := en_US en_GB fr_FR it_IT de_DE es_ES hdpi

# Additional settings used in the builds
PRODUCT_PROPERTY_OVERRIDES += \
        keyguard.no_require_sim=true \
        ro.com.android.dateformat=MM-dd-yyyy 

PRODUCT_COPY_FILES += \
         $(LOCAL_PATH)/apns.xml:system/etc/apns-conf.xml 
PRODUCT_COPY_FILES += \
         $(LOCAL_PATH)/system/media/audio/notifications/android_on_freerunner_notification.ogg:system/media/audio/notifications/android_on_freerunner_notification.ogg
PRODUCT_COPY_FILES += \
         $(LOCAL_PATH)/system/media/audio/ringtones/android_on_freerunner_ringtone.ogg:system/media/audio/ringtones/android_on_freerunner_ringtone.ogg

# Pick up some sounds
include frameworks/base/data/sounds/AudioPackage2.mk

# TTS languages
include external/svox/pico/lang/PicoLangDeDeInSystem.mk
include external/svox/pico/lang/PicoLangEnGBInSystem.mk
include external/svox/pico/lang/PicoLangEnUsInSystem.mk
include external/svox/pico/lang/PicoLangEsEsInSystem.mk
include external/svox/pico/lang/PicoLangFrFrInSystem.mk
include external/svox/pico/lang/PicoLangItItInSystem.mk

